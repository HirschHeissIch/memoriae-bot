Memoriae Bot
============

.. image:: https://img.shields.io/badge/python-3-blue
   :target: https://www.python.org/doc/versions/
   :alt: Supported Python versions

.. image:: https://img.shields.io/badge/backend-python--telegram--bot-blue
   :target: https://python-telegram-bot.org/
   :alt: Backend: python-telegram-bot

.. image:: https://img.shields.io/badge/documentation-is%20here-orange
   :target: https://hirschheissich.gitlab.io/memoriae-bot
   :alt: Documentation

.. image:: https://img.shields.io/badge/chat%20on-Telegram-blue
   :target: https://t.me/memoriae_bot
   :alt: Telegram Chat

Memoriae Bot is a Telegram Bot that acts as a wrapper for Telegrams timed messages that allows you to
postpone reminders, when you get them.
You can find it at `@memoriae_bot`_.

.. _`@memoriae_bot`: https://t.me/memoriae_bot

Copyright
---------
The logo is based on `this one`_ made by `Dimi Kazak`_ from `Flaticon`_.

.. _`this one`: https://www.flaticon.com/free-icon/hat_1177686
.. _`Dimi Kazak`: https://www.flaticon.com/authors/freepik
.. _`Flaticon`: https://www.flaticon.com/
