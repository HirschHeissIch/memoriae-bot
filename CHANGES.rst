=========
Changelog
=========

Version 1.1
===========
*Released 2020-08-17*

**Enhancements:**

* Include the `ptbstats <https://hirschheissich.gitlab.io/ptbstats/>`_ plugin for statistics

Version 1.0.1
=============
*Released 2020-03-18*

**New features**

* Use config file ``bot.ini`` for customizing bot token and admins chat id

Version 1.0
===========
*Released 2020-03-17*

**New features**

* Simple bot commands for showing info about the bot
* Postponing functionality